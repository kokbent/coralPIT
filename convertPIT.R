convertPIT <- function (dat, interval, start = "start", end = "end", 
                         column = "all") {
  # Convert LIT data (dat) to PIT data and return the PIT data
  # as a dataframe. User needs to specify the interval
  
  # extract the start and end column
  start = dat[,start]
  end = dat[,end]
  
  # Check if the new table to be returned should include
  # all or part of the columns specify by user
  if (length(column) == 1) {
    if (column == "all") datSubset <- dat else {
      datSubset <- as.data.frame(dat[,column])
      colnames(datSubset) <- column
    }
  } else datSubset <- dat[,column]
  
  # Creating the point intercepts
  maxLen <- max(end)
  pointInterv <- seq(0, maxLen, by = interval)
  index <- rep(NA, length(pointInterv))
  
  # Check where in the data does the point intercept lies
  # If the intercept is on the end point of one interval and start
  # of the other, mark it as belonging to the latter except the last
  # point
  for (i in 1:length(pointInterv)) {
    intervalCheck <- pointInterv[i] >= start & pointInterv[i] < end
    if (pointInterv[i] == maxLen) {
      index[i] <- length(end)
    } else if (sum(intervalCheck) > 1 || sum(intervalCheck) < 1) {
      shout <- paste("Something is wrong at point ", pointInterv[i])
      print(shout)
    } else index[i] <- which(intervalCheck)
  }
  
  newDat <- datSubset[index,]
  newDat <- cbind(point = pointInterv, newDat)
  return(newDat)
}