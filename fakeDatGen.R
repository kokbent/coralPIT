# Fake data generation
rm(list=ls())
transLen <- 10000
interval <- 300
bentCatExample <- c("A", "B", "C", "D", "E", "F", "G", "H", "I")
randRemExample <- c("AA", "BB", "CC", "DD", "EE")
benthicCat <- randomRemark <- rep(NA, interval)

start <- end <- rep(NA, interval)
start[1] <- 0
end[interval] <- transLen

ind <- sample(1:(transLen-1), interval-1, replace = F)
ind <- sort(ind)
start[2:interval] <- ind
end[1:(interval-1)] <- ind

for (i in 1:interval) {
  bentCatIncl <- bentCatExample
  if (i > 1) {
    tmp <- which(!(bentCatExample %in% benthicCat[i-1]))
    bentCatIncl <- bentCatExample[tmp]
  }
  
  benthicCat[i] <- sample(bentCatIncl, 1)
  randomRemark[i] <- sample(randRemExample, 1)
}

dat <- data.frame(start=start, end=end, benthicCat=benthicCat, randomRemark=randomRemark)
write.csv(dat, "fakeData.csv", row.names = F, quote = F)
